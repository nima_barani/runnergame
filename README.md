# Endless 3D Runner Game in Unity

In this project, we designed and developed a runner game, like Subway Surfers, in Unity. It never ends unless the player loses- colliding to obstacles. In order to make it endless, reduce both memory and battery usage, and increase FPS we generate the ground and the obstacles automatically and randomly.

![GamePlay](https://media.giphy.com/media/TI9xArq4uEZVSwxAM2/source.gif)

## Running the tests
We built a version of game for Windows which is in *WindowsBuilt* folder.

## Authors

* [**Nima Barani**](https://gitlab.com/nima_barani)
* [**Mohammadreza Tajerian**](https://gitlab.com/momad200)
